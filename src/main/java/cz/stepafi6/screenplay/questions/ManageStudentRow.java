package cz.stepafi6.screenplay.questions;

import cz.stepafi6.screenplay.user_interface.StudentManagement;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

public class ManageStudentRow {
    public static Question<String> forenameOf(String username) {
        return Text.of(StudentManagement.MANAGE_STUDENT_ROW_FORENAME.of(username))
            .describedAs("student forename")
            .asAString();
    }
}
