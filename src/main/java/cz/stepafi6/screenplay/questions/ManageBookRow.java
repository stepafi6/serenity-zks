package cz.stepafi6.screenplay.questions;

import cz.stepafi6.screenplay.user_interface.BookManagement;
import cz.stepafi6.screenplay.user_interface.StudentManagement;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class ManageBookRow {
    public static Question<String> authorOf(String title) {
        return Text.of(BookManagement.MANAGE_BOOK_ROW_AUTHOR.of(title))
            .describedAs("book author")
            .asAString();
    }
}
