package cz.stepafi6.screenplay.questions;

import cz.stepafi6.screenplay.user_interface.StudentManagement;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;

@Subject("the manage student row")
public class ManageStudentRowAvailable implements Question<ElementAvailability> {
    String username;

    public ManageStudentRowAvailable(String username) {
        this.username = username;
    }

    @Override
    public ElementAvailability answeredBy(Actor actor) {
        return ElementAvailability.from(
            StudentManagement.MANAGE_STUDENT_ROW.of(username).resolveFor(actor).isCurrentlyVisible()
        );
    }

    public static ManageStudentRowAvailable ofUsername(String username) {
        return new ManageStudentRowAvailable(username);
    }
}
