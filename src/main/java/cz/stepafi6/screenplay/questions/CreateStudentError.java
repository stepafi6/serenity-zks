package cz.stepafi6.screenplay.questions;

import cz.stepafi6.screenplay.user_interface.CreateStudentForm;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

@Subject("the create student errors")
public class CreateStudentError implements Question<ElementAvailability> {

    @Override
    public ElementAvailability answeredBy(Actor actor) {
        return ElementAvailability.from(
            CreateStudentForm.ERROR.resolveFor(actor).isCurrentlyVisible()
        );
    }

    public static CreateStudentError errorBox() {
        return new CreateStudentError();
    }
}
