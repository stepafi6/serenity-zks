package cz.stepafi6.screenplay.questions;

import cz.stepafi6.screenplay.user_interface.BookManagement;
import cz.stepafi6.screenplay.user_interface.StudentManagement;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;

@Subject("the manage book row")
public class ManageBookRowAvailable implements Question<ElementAvailability> {
    String title;

    public ManageBookRowAvailable(String title) {
        this.title = title;
    }

    @Override
    public ElementAvailability answeredBy(Actor actor) {
        return ElementAvailability.from(
            BookManagement.MANAGE_BOOK_ROW.of(title).resolveFor(actor).isCurrentlyVisible()
        );
    }

    public static ManageBookRowAvailable ofTitle(String title) {
        return new ManageBookRowAvailable(title);
    }
}
