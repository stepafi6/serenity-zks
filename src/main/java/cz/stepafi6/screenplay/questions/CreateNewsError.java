package cz.stepafi6.screenplay.questions;

import cz.stepafi6.screenplay.user_interface.CreateNewsForm;
import cz.stepafi6.screenplay.user_interface.CreateStudentForm;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;

@Subject("the create news errors")
public class CreateNewsError implements Question<ElementAvailability> {

    @Override
    public ElementAvailability answeredBy(Actor actor) {
        return ElementAvailability.from(
            CreateNewsForm.ERROR.resolveFor(actor).isCurrentlyVisible()
        );
    }

    public static CreateNewsError errorBox() {
        return new CreateNewsError();
    }
}
