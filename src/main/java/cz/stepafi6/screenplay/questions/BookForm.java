package cz.stepafi6.screenplay.questions;

import cz.stepafi6.screenplay.user_interface.CreateBookForm;
import cz.stepafi6.screenplay.user_interface.StudentManagement;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.questions.Value;

public class BookForm {
    public static Question<String> title() {
        return Value.of(CreateBookForm.TITLE)
            .describedAs("books title")
            .asAString();
    }
}
