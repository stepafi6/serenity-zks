package cz.stepafi6.screenplay.questions;

import cz.stepafi6.screenplay.user_interface.CreateNewsForm;
import cz.stepafi6.screenplay.user_interface.NewsManagement;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;

@Subject("the card with title")
public class ExistsNewsCard implements Question<ElementAvailability> {
    String title;

    public ExistsNewsCard(String title) {
        this.title = title;
    }

    @Override
    public ElementAvailability answeredBy(Actor actor) {
        return ElementAvailability.from(
            NewsManagement.MANAGE_NEWS_TITLE.of(title).resolveFor(actor).isCurrentlyVisible()
        );
    }

    public static ExistsNewsCard withTitle(String title) {
        return new ExistsNewsCard(title);
    }
}
