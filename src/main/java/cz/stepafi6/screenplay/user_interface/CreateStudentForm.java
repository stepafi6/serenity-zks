package cz.stepafi6.screenplay.user_interface;

import net.serenitybdd.screenplay.targets.Target;

public class CreateStudentForm {
    public static final Target STUDENT_FORENAME = Target.the("'Jméno' field").locatedBy("#forenameInput");
    public static final Target STUDENT_SURNAME = Target.the("'Příjmení' field").locatedBy("#surnameInput");
    public static final Target STUDENT_USERNAME = Target.the("'Uživatelské jméno' field").locatedBy("#usernameInput");
    public static final Target STUDENT_CLASS = Target.the("'Třída' field").locatedBy("#classInput");
    public static final Target STUDENT_EMAIL = Target.the("'Email' field").locatedBy("#emailInput");
    public static final Target STUDENT_PASSWORD = Target.the("'Heslo' field").locatedBy("#passwordInput");
    public static final Target STUDENT_PASSWORD_REPEAT = Target.the("'Heslo znovu' field").locatedBy("#passwordRepeatInput");
    public static final Target CREATE_STUDENT_BUTTON = Target.the("create student button").locatedBy("//input[@type=\"submit\"]");

    public static final Target ERROR = Target.the("create student errors").locatedBy("//div[@role='alert']");
}
