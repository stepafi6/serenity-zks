package cz.stepafi6.screenplay.user_interface;

import net.serenitybdd.screenplay.targets.Target;

public class LoginForm {
    public static final Target LOGIN_USERNAME = Target.the("'Uživatelské jméno' field").locatedBy("#username");
    public static final Target LOGIN_PASSWORD = Target.the("'Heslo' field").locatedBy("#password");
    public static final Target LOGIN_BUTTON = Target.the("login button").locatedBy("//button[@type=\"submit\"]");
}
