package cz.stepafi6.screenplay.user_interface;

import net.serenitybdd.screenplay.targets.Target;

public class BookManagement {
    public static final Target EDIT_BOOK_BUTTON = Target.the("delete book {0} button").locatedBy("//td[text()='{0}']/../td[last()]/a[@aria-label='Upravit']");
    public static final Target MANAGE_BOOK_ROW_AUTHOR = Target.the("book {0} manage row").locatedBy("//td[text()='stepanek.filip']/../td[2]");

    public static final Target DELETE_BOOK_BUTTON = Target.the("delete book {0} button").locatedBy("//td[text()='{0}']/../td[last()]/a[@aria-label='Odstranit']");
    public static final Target MANAGE_BOOK_ROW = Target.the("book {0} manage row").locatedBy("//td[text()='{0}']/..");
}
