package cz.stepafi6.screenplay.user_interface;

import net.serenitybdd.screenplay.targets.Target;

public class CreateNewsForm {
    public static final Target NEWS_TITLE = Target.the("'Nadpis' field").locatedBy("#titleInput");
    public static final Target NEWS_CONTENT = Target.the("'Obsah' field").locatedBy("#contentTextarea");
    public static final Target NEWS_FROM = Target.the("'Platné od' field").locatedBy("#validFromInput");
    public static final Target NEWS_TO = Target.the("'Platné do' field").locatedBy("#validToInput");

    public static final Target CREATE_NEWS_BUTTON = Target.the("create news button").locatedBy("//input[@type=\"submit\"]");

    public static final Target ERROR = Target.the("create news errors").locatedBy("//div[@role='alert']");
}
