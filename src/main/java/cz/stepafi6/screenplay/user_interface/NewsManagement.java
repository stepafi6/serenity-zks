package cz.stepafi6.screenplay.user_interface;

import net.serenitybdd.screenplay.targets.Target;

public class NewsManagement {
    public static final Target EDIT_NEWS_BUTTON = Target.the("edit news with title {0} button").locatedBy("//h5[@class='card-title' and text()='{0}']/../..//a[text()='Upravit']");
    public static final Target DELETE_NEWS_BUTTON = Target.the("delete news with title {0} button").locatedBy("//h5[@class='card-title' and text()='{0}']/../..//a[text()='Odstranit']");
    public static final Target MANAGE_NEWS_TITLE = Target.the("student {0} manage row").locatedBy("//h5[@class='card-title' and text()='{0}']");
}
