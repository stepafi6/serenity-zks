package cz.stepafi6.screenplay.user_interface;

import net.serenitybdd.screenplay.targets.Target;

public class CreateBookForm {
    public static final Target ISBN_SEARCH = Target.the("'Zadejte ISBN' field").locatedBy("#isbnSearchInput");
    public static final Target TITLE = Target.the("'Název' field").locatedBy("#titleInput");
    public static final Target ISBN_SEARCH_BUTTON = Target.the("search isbn button").locatedBy("//button[@value='Vyhledat']");
    public static final Target AUTHOR = Target.the("'Autor' field").locatedBy("#authorInput");
    public static final Target CREATE_BOOK_BUTTON = Target.the("search isbn button").locatedBy("//input[@type='submit']");
}
