package cz.stepafi6.screenplay.user_interface;

import net.serenitybdd.screenplay.targets.Target;

public class StudentManagement {
    public static final Target EDIT_STUDENT_BUTTON = Target.the("delete student {0} button").locatedBy("//td[text()='{0}']/../td[last()]/a[@aria-label='Upravit']");
    public static final Target MANAGE_STUDENT_ROW_FORENAME = Target.the("student {0} manage row").locatedBy("//td[text()='stepanek.filip']/../td[2]");

    public static final Target DELETE_STUDENT_BUTTON = Target.the("delete student {0} button").locatedBy("//td[text()='{0}']/../td[last()]/a[@aria-label='Odstranit']");
    public static final Target MANAGE_STUDENT_ROW = Target.the("student {0} manage row").locatedBy("//td[text()='{0}']/..");
}
