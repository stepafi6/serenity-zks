package cz.stepafi6.screenplay.tasks;

import cz.stepafi6.screenplay.user_interface.CreateNewsForm;
import cz.stepafi6.screenplay.user_interface.NewsManagement;
import cz.stepafi6.screenplay.user_interface.StudentManagement;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SendKeys;
import net.serenitybdd.screenplay.actions.SendKeysIntoTarget;

import static cz.stepafi6.screenplay.user_interface.CreateNewsForm.*;
import static cz.stepafi6.screenplay.user_interface.CreateStudentForm.STUDENT_FORENAME;

public class ManageNews {
    //Create
    public static Task fillTitle(String title) {
        return Task.where("{0} fill news attribute title: #title",
            Enter.theValue(title)
                .into(NEWS_TITLE)
        ).with("title").of(title);
    }

    public static Task fillContent(String content) {
        return Task.where("{0} fill news attribute content: #content",

            Enter.theValue(content)
                .into(NEWS_CONTENT)
        ).with("content").of(content);
    }

    public static Task fillFrom(String from) {
        return Task.where("{0} fill news attribute from: #from",
            SendKeys.of(from)
                .into(NEWS_FROM)
        ).with("from").of(from);
    }

    public static Task fillTo(String to) {
        return Task.where("{0} fill news attribute from: #to",
                SendKeys.of(to)
                .into(NEWS_TO)
        ).with("to").of(to);
    }

    public static Task confirmCreation() {
        return Task.where("{0} click the create news button",
            Click.on(CREATE_NEWS_BUTTON)
        );
    }

    //Update
    public static Performable enterNewsEdit(String title) {
        return Task.where("{0} click the update news with title: #title",
            Click.on(NewsManagement.EDIT_NEWS_BUTTON.of(title))
        ).with("title").of(title);
    }

    public static Performable updateTitle(String newTitle) {
        return Task.where("{0} update news attribute title: #newTitle",
            Enter.theValue(newTitle)
                .into(NEWS_TITLE)
        ).with("newTitle").of(newTitle);
    }

    //Delete
    public static Performable deleteNews(String title) {
        return Task.where("{0} click the delete news with title: #title",
            Click.on(NewsManagement.DELETE_NEWS_BUTTON.of(title))
        ).with("title").of(title);
    }
}
