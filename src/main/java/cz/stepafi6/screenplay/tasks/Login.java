package cz.stepafi6.screenplay.tasks;

import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static cz.stepafi6.screenplay.user_interface.LoginForm.*;
import static org.openqa.selenium.Keys.TAB;

public class Login {

    public static Task FillUsername(String username) {
        return Task.where("{0} fill the username: #username",
            Enter.theValue(username)
                .into(LOGIN_USERNAME)
        ).with("username").of(username);
    }

    public static Task FillPassword(String password) {
        return Task.where("{0} fill the password: #password",
            Enter.theValue(password)
                .into(LOGIN_PASSWORD)
        ).with("password").of(password);
    }

    public static Task LoginButton() {
        return Task.where("{0} click the login button",
            Click.on(LOGIN_BUTTON)
        );
    }
}
