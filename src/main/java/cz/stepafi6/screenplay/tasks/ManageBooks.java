package cz.stepafi6.screenplay.tasks;

import cz.stepafi6.screenplay.user_interface.BookManagement;
import cz.stepafi6.screenplay.user_interface.StudentManagement;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static cz.stepafi6.screenplay.user_interface.CreateBookForm.*;
import static cz.stepafi6.screenplay.user_interface.CreateStudentForm.STUDENT_FORENAME;


public class ManageBooks {
    //Create
    public static Performable fillIsbnSearch(String isbn) {
        return Task.where("{0} fill isbn search: #isbn",
            Enter.theValue(isbn)
                .into(ISBN_SEARCH)
        ).with("isbn").of(isbn);
    }

    public static Performable searchIsbn() {
        return Task.where("{0} click the create news button",
            Click.on(ISBN_SEARCH_BUTTON)
        );
    }

    public static Performable confirmCreation() {
        return Task.where("{0} click the create book button",
            Click.on(CREATE_BOOK_BUTTON)
        );
    }
    //Update
    public static Performable enterBookEdit(String title) {
        return Task.where("{0} click the delete book with title: #title",
            Click.on(BookManagement.EDIT_BOOK_BUTTON.of(title))
        ).with("title").of(title);
    }

    public static Performable updateAuthor(String author) {
        return Task.where("{0} update book attribute author: #author",
            Enter.theValue(author)
                .into(AUTHOR)
        ).with("author").of(author);
    }

    //Delete
    public static Performable deleteBook(String title) {
        return Task.where("{0} click the delete book with title: #title",
            Click.on(BookManagement.DELETE_BOOK_BUTTON.of(title))
        ).with("title").of(title);
    }
}
