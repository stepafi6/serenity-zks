package cz.stepafi6.screenplay.tasks;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

public class Start {
    public static Performable withCreateStudentPage() {
        return Task.where("{0} starts with an empty create student page",
            Open.browserOn().thePageNamed("teacher.createStudent")
        );
    }

    public static Performable withLoginPage() {
        return Task.where("{0} starts with login page",
            Open.browserOn().thePageNamed("home.login")
        );
    }

    public static Performable withManageStudents() {
        return Task.where("{0} starts with an manage students page",
            Open.browserOn().thePageNamed("teacher.manageStudent")
        );
    }

    public static Performable withCreateNewsPage() {
        return Task.where("{0} starts with an empty create student page",
            Open.browserOn().thePageNamed("teacher.createNews")
        );
    }

    public static Performable withManageNews() {
        return Task.where("{0} starts with an empty create student page",
            Open.browserOn().thePageNamed("teacher.manageNews")
        );
    }

    public static Performable withCreateBookPage() {
        return Task.where("{0} starts with an empty create student page",
            Open.browserOn().thePageNamed("teacher.createBook")
        );
    }

    public static Performable withManageBooks() {
        return Task.where("{0} starts with an empty create student page",
            Open.browserOn().thePageNamed("teacher.manageBook")
        );
    }
}
