package cz.stepafi6.screenplay.tasks;

import cz.stepafi6.screenplay.user_interface.StudentManagement;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static cz.stepafi6.screenplay.user_interface.CreateStudentForm.*;


public class ManageStudents {
    //Create student
    public static Task fillForename(String forename) {
        return Task.where("{0} fill student attribute forename: #forename",
            Enter.theValue(forename)
                .into(STUDENT_FORENAME)
        ).with("forename").of(forename);
    }

    public static Task fillSurname(String surname) {
        return Task.where("{0} fill student attribute surname: #surname",
            Enter.theValue(surname)
                .into(STUDENT_SURNAME)
        ).with("surname").of(surname);
    }

    public static Task fillUsername(String username) {
        return Task.where("{0} fill student attribute username: #username",
            Enter.theValue(username)
                .into(STUDENT_USERNAME)
        ).with("username").of(username);
    }

    public static Task fillClass(String xclass) {
        return Task.where("{0} fill student attribute class: #xclass",
            Enter.theValue(xclass)
                .into(STUDENT_CLASS)
        ).with("xclass").of(xclass);
    }

    public static Task fillEmail(String email) {
        return Task.where("{0} fill student attribute email: #email",
            Enter.theValue(email)
                .into(STUDENT_EMAIL)
        ).with("email").of(email);
    }

    public static Task fillPassword(String password) {
        return Task.where("{0} fill student attribute password: #password",
            Enter.theValue(password)
                .into(STUDENT_PASSWORD)
                .then(
                    Enter.theValue(password)
                        .into(STUDENT_PASSWORD_REPEAT)
                )
        ).with("password").of(password);
    }

    public static Task confirmCreation() {
        return Task.where("{0} click the create student button",
            Click.on(CREATE_STUDENT_BUTTON)
        );
    }

    //Update student
    public static Performable enterStudentEdit(String username) {
        return Task.where("{0} click the delete student with username: #username",
            Click.on(StudentManagement.EDIT_STUDENT_BUTTON.of(username))
        ).with("username").of(username);
    }

    public static Performable updateForename(String forename) {
        return Task.where("{0} update student attribute forename: #forename",
            Enter.theValue(forename)
                .into(STUDENT_FORENAME)
        ).with("forename").of(forename);
    }

    //Delete student
    public static Performable deleteStudent(String username) {
        return Task.where("{0} click the delete student with username: #username",
            Click.on(StudentManagement.DELETE_STUDENT_BUTTON.of(username))
        ).with("username").of(username);
    }
}
