Feature: Manage students

    As James (teacher)
    I want to manage students

    Scenario: Create student
        Given that James was able to log in
        And that he can enter create student page
        When he fill test attributes and click create
        Then he can't see any error

    Scenario: Update student
        Given that James was able to log in
        And that he can enter student management
        When he clicks on edit student
        And he updates student name
        Then he should see new name

    Scenario: Delete student
        Given that James was able to log in
        And that he can enter student management
        When he click on delete student
        Then he can't see the deleted student anymore
