Feature: Manage news

    As James (teacher)
    I want to manage news

    Scenario: Create news
        Given that James was able to log in
        And that he can enter create actuality page
        When he fill actuality attributes and click create
        Then he can see new actuality

    Scenario: Update news
        Given that James was able to log in
        And that he can enter news management
        When he clicks on edit news
        And he updates news title
        Then he should see updated title

    Scenario: Delete news
        Given that James was able to log in
        And that he can enter news management
        When he clicks on delete news
        Then he shouldn't see deleted news anymore
