Feature: Manage books

    As James (teacher)
    I want to manage books

    Scenario: Create book
        Given that James was able to log in
        And that he can enter create book page
        When he fill isbn and click search
        And he clicks create book
        Then he can't see any books error

    Scenario: Update book
        Given that James was able to log in
        And that he can enter books management
        When he clicks on edit book
        And he updates book author
        Then he can't see any books error

    Scenario: Delete news
        Given that James was able to log in
        And that he can enter books management
        When he clicks on delete book
        Then he shouldn't see deleted book anymore
