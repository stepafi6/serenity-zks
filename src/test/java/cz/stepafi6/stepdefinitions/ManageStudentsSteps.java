package cz.stepafi6.stepdefinitions;

import cz.stepafi6.screenplay.questions.CreateStudentError;
import cz.stepafi6.screenplay.questions.ManageStudentRow;
import cz.stepafi6.screenplay.questions.ManageStudentRowAvailable;
import cz.stepafi6.screenplay.tasks.ManageStudents;
import cz.stepafi6.screenplay.tasks.Login;
import cz.stepafi6.screenplay.tasks.Start;
import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static cz.stepafi6.screenplay.questions.ElementAvailability.Unavailable;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.setTheStage;
import static org.hamcrest.Matchers.*;

public class ManageStudentsSteps {

    //Create
    @Given("that {actor} can enter create student page")
    public void thatJamesCanEnterMainPage(Actor actor) {
        actor.wasAbleTo(Start.withCreateStudentPage());
    }

    @When("{actor} fill test attributes and click create")
    public void heSelectCategory(Actor actor) {
        actor.attemptsTo(ManageStudents.fillForename("Filip"));
        actor.attemptsTo(ManageStudents.fillSurname("Štěpánek"));
        actor.attemptsTo(ManageStudents.fillUsername("stepanek.filip"));
        actor.attemptsTo(ManageStudents.fillClass("8.B"));
        actor.attemptsTo(ManageStudents.fillEmail("stepanek@example.com"));
        actor.attemptsTo(ManageStudents.fillPassword("123456789"));

        actor.attemptsTo(ManageStudents.confirmCreation());
    }

    @Then("{actor} can't see any error")
    public void heCanSeeItemsUnderThisCategory(Actor actor) {
        actor.should(seeThat(CreateStudentError.errorBox(), is(Unavailable)));
    }

    //Update
    @And("that {actor} can enter student management")
    public void thatHeCanEnterStudentManagement(Actor actor) {
        actor.wasAbleTo(Start.withManageStudents());
    }

    @When("{actor} clicks on edit student")
    public void heClicksOnEditStudent(Actor actor) {
        actor.attemptsTo(ManageStudents.enterStudentEdit("stepanek.filip"));
    }

    @And("{actor} updates student name")
    public void heUpdatesStudentName(Actor actor) {
        actor.attemptsTo(ManageStudents.updateForename("Ignác"));
        actor.attemptsTo(ManageStudents.confirmCreation());
    }

    @Then("{actor} should see new name")
    public void heShouldSeeNewName(Actor actor) {
        actor.should(seeThat(ManageStudentRow.forenameOf("stepanek.filip"), is("Ignác")));
    }

    //Delete
    @When("{actor} click on delete student")
    public void heClickOnDeleteFirstStudent(Actor actor) {
        actor.attemptsTo(ManageStudents.deleteStudent("stepanek.filip"));
    }

    @Then("{actor} can't see the deleted student anymore")
    public void heCanTSeeTheDeletedStudentAnymore(Actor actor) {
        actor.should(seeThat(ManageStudentRowAvailable.ofUsername("stepanek.filip"), is(Unavailable)));
    }
}
