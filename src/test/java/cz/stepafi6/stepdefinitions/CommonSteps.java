package cz.stepafi6.stepdefinitions;

import cz.stepafi6.screenplay.tasks.Login;
import cz.stepafi6.screenplay.tasks.Start;
import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Given;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.actors.OnStage.setTheStage;

public class CommonSteps {
    @Before
    public void set_the_stage() {
        setTheStage(new OnlineCast());
    }

    @ParameterType(".*")
    public Actor actor(String actor) {
        return OnStage.theActorCalled(actor);
    }

    @Given("that {actor} was able to log in")
    public void thatJamesWasAbleToLogIn(Actor actor) {
        actor.wasAbleTo(Start.withLoginPage());

        actor.wasAbleTo(Login.FillUsername("novak.jan"));
        actor.wasAbleTo(Login.FillPassword("123456789"));
        actor.wasAbleTo(Login.LoginButton());
    }
}
