package cz.stepafi6.stepdefinitions;

import cz.stepafi6.screenplay.questions.*;
import cz.stepafi6.screenplay.tasks.ManageBooks;
import cz.stepafi6.screenplay.tasks.ManageStudents;
import cz.stepafi6.screenplay.tasks.Start;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.waits.Wait;

import static cz.stepafi6.screenplay.questions.ElementAvailability.Unavailable;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.*;

public class ManageBooksSteps {
    //Create
    @And("that {actor} can enter create book page")
    public void thatHeCanEnterCreateBookPage(Actor actor) {
        actor.wasAbleTo(Start.withCreateBookPage());
    }

    @When("{actor} fill isbn and click search")
    public void heFillIsbnAndClickSearch(Actor actor) throws InterruptedException {
        actor.attemptsTo(ManageBooks.fillIsbnSearch("80-00-00586-7"));

        actor.attemptsTo(ManageBooks.searchIsbn());
        actor.attemptsTo(Wait.until(
           BookForm.title(),not(emptyOrNullString())
        ).forNoLongerThan(30).seconds());
    }

    @And("{actor} clicks create book")
    public void heClicksCreateBook(Actor actor) {
        actor.attemptsTo(ManageBooks.confirmCreation());
    }


    @Then("{actor} can't see any books error")
    public void heCanTSeeAnyBooksError(Actor actor) {
    }

    //Update
    @And("that {actor} can enter books management")
    public void thatHeCanEnterBooksManagement(Actor actor) {
        actor.wasAbleTo(Start.withManageBooks());
    }

    @When("{actor} clicks on edit book")
    public void heClicksOnEditBook(Actor actor) {
        actor.attemptsTo(ManageBooks.enterBookEdit("Malý princ"));
    }

    @And("{actor} updates book author")
    public void heUpdatesBookAuthor(Actor actor) {
        actor.attemptsTo(ManageBooks.updateAuthor("Bůh umění"));
        actor.attemptsTo(ManageBooks.confirmCreation());
    }

    //Delete
    @When("{actor} clicks on delete book")
    public void heClicksOnDeleteBook(Actor actor) {
        actor.attemptsTo(ManageBooks.deleteBook("Malý princ"));
    }

    @Then("{actor} shouldn't see deleted book anymore")
    public void heShouldnTSeeDeletedBookAnymore(Actor actor) {
        actor.should(seeThat(ManageBookRowAvailable.ofTitle("Malý princ"), is(Unavailable)));
    }
}
