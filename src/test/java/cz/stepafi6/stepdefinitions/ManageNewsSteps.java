package cz.stepafi6.stepdefinitions;

import cz.stepafi6.screenplay.questions.*;
import cz.stepafi6.screenplay.tasks.ManageNews;
import cz.stepafi6.screenplay.tasks.ManageStudents;
import cz.stepafi6.screenplay.tasks.Start;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;

import static cz.stepafi6.screenplay.questions.ElementAvailability.Available;
import static cz.stepafi6.screenplay.questions.ElementAvailability.Unavailable;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.is;

public class ManageNewsSteps {
    //Create
    @Given("that {actor} can enter create actuality page")
    public void thatJamesCanEnterMainPage(Actor actor) {
        actor.wasAbleTo(Start.withCreateNewsPage());
    }

    @When("{actor} fill actuality attributes and click create")
    public void heFillActualityAttributesAndClickCreate(Actor actor) {
        actor.attemptsTo(ManageNews.fillTitle("Testovací nadpis"));
        actor.attemptsTo(ManageNews.fillContent("Lorem ipsum dolor san"));
        actor.attemptsTo(ManageNews.fillFrom("01122021"));

        actor.attemptsTo(ManageNews.confirmCreation());
    }

    @Then("{actor} can see new actuality")
    public void heCanSeeNewActuality(Actor actor) {
        actor.should(seeThat(CreateNewsError.errorBox(), is(Unavailable)));
    }

    //Update
    @And("that {actor} can enter news management")
    public void thatHeCanEnterNewsManagement(Actor actor) {
        actor.wasAbleTo(Start.withManageNews());
    }

    @When("{actor} clicks on edit news")
    public void heClicksOnEditNews(Actor actor) {
        actor.attemptsTo(ManageNews.enterNewsEdit("Testovací nadpis"));
    }

    @And("{actor} updates news title")
    public void heUpdatesNewsTitle(Actor actor) {
        actor.attemptsTo(ManageNews.updateTitle("Nový nadpis"));
        actor.attemptsTo(ManageNews.confirmCreation());
    }

    @Then("{actor} should see updated title")
    public void heShouldSeeUpdatedTitle(Actor actor) {
        actor.should(seeThat(ExistsNewsCard.withTitle("Nový nadpis"), is(Available)));
    }

    //Delete
    @When("{actor} clicks on delete news")
    public void heClicksOnDeleteNews(Actor actor) {
        actor.attemptsTo(ManageNews.deleteNews("Nový nadpis"));
    }

    @Then("{actor} shouldn't see deleted news anymore")
    public void heShouldnTSeeDeletedNewsAnymore(Actor actor) {
        actor.should(seeThat(ExistsNewsCard.withTitle("Nový nadpis"), is(Unavailable)));
    }
}
