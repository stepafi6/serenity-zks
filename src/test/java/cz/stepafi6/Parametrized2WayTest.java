package cz.stepafi6;

import cz.stepafi6.screenplay.questions.CreateNewsError;
import cz.stepafi6.screenplay.questions.ElementAvailability;
import cz.stepafi6.screenplay.tasks.Login;
import cz.stepafi6.screenplay.tasks.ManageNews;
import cz.stepafi6.screenplay.tasks.Start;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import net.thucydides.junit.annotations.Concurrent;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.is;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom(value = "src/test/resources/testSet2Way.csv")
public class Parametrized2WayTest {
    private Actor james = Actor.named("James");

    String title;
    String content;
    String validfrom;
    String validto;
    boolean result;

    @Managed
    private WebDriver hisBrowser;

    @Before
    public void jamesCanBrowseTheWeb() {
        james.can(BrowseTheWeb.with(hisBrowser));
    }

    @Test
    public void create_events_from_csv() {
        givenThat(james).wasAbleTo(
                Start.withLoginPage(),
                Login.FillUsername("novak.jan"),
                Login.FillPassword("123456789"),
                Login.LoginButton()
        );
        andThat(james).wasAbleTo(Start.withCreateNewsPage());

        when(james).attemptsTo(
                ManageNews.fillTitle(title),
                ManageNews.fillContent(content),
                ManageNews.fillFrom(validfrom),
                ManageNews.fillTo(validto)
        );
        when(james).attemptsTo(ManageNews.confirmCreation());


        then(james).should(seeThat(CreateNewsError.errorBox(), is(ElementAvailability.from(!result))));
    }

    public void setTitle(String title) {
        if (title.equals("null"))
            title = "";

        this.title = title;
    }

    public void setContent(String content) {
        if (content.equals("null"))
            content = "";

        this.content = content;
    }

    public void setValidfrom(String validfrom) {
        if (validfrom.equals("null"))
            validfrom = "";

        this.validfrom = validfrom;
    }

    public void setValidto(String validto) {
        if (validto.equals("null"))
            validto = "";

        this.validto = validto;
    }

    public void setResult(String result) {
        this.result = Boolean.parseBoolean(result);
    }
}
