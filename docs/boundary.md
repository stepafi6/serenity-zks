# Knihovna
## News
### Title
Descriptive title of the news
#### ECs
|null| 1 <= length(title) <= 50 | length(title) > 50 |
|--|--|--|
| Invalid | Valid | Invalid |
#### Boundary values
null, y, yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy, xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
### Content
News text xontent
#### ECs
|null| 1 <= length(content) <= 2000 | length(content) > 2000 |
|--|--|--|
| Invalid | Valid | Invalid |
#### Boundary values
null, y, {2000x"y"}, {2001*"x"}

### From
From when is the news valid
#### ECs
|null| (any number) | (any text) | ^\s*(3[01]\|[12][0-9]\|0?[1-9])\.(1[012]\|0?[1-9])\.((?:19\|20)\d{2})\s*$ |
|--|--|--|--|
| Invalid | Invalid | Invalid | Valid |
#### Boundary values
null, xxx, 0, 1.12.2021

### To
Until when is the news valid
#### ECs
|null| (any number) | (any text) | ^\s*(3[01]\|[12][0-9]\|0?[1-9])\.(1[012]\|0?[1-9])\.((?:19\|20)\d{2})\s*$ |
|--|--|--|--|
| Valid | Invalid | Invalid | Valid |
#### Boundary values
null, xxx, 0, 1.12.2021